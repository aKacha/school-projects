﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace drawing
{
    public class drawmats
    {

        // maakt een cirkel aan
        public static void mCircle(System.Drawing.Point point, int radius, System.Drawing.Color color, int width, Form1 form)
        {
            System.Drawing.Pen objPen = new System.Drawing.Pen(color);
            System.Drawing.Graphics paper1 = form.CreateGraphics();
            paper1.DrawEllipse(objPen, point.X - radius, point.Y - radius, 2 * radius, 2 * radius);
        }

        // maakt een lijn aan
        public static void mLine(System.Drawing.Point point1, System.Drawing.Point point2, System.Drawing.Color color2, int width2, Form1 form2)
        {
            System.Drawing.Pen objPen2 = new System.Drawing.Pen(color2, width2);
            System.Drawing.Graphics paper2 = form2.CreateGraphics();
            paper2.DrawLine(objPen2, point1, point2);
        }
    }
}
