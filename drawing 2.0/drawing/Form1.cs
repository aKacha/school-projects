﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace drawing
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int width = 3;
        Point pPoint, rPoint;

        // tekent cirkel wanneer je op muis klikt
        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            drawmats.mCircle(e.Location, 6, Color.Blue, 3, this);
            if (pPoint != rPoint)
            {
                drawmats.mLine(e.Location, pPoint, Color.Red, 2 * width, this);
            }
            
            else if(Button.MouseButtons == MouseButtons.Middle)
            {

            }

            pPoint = e.Location;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (Button.MouseButtons == MouseButtons.Right)
            {
                drawmats.mLine(e.Location, pPoint, Color.Green, 2 * width, this);
            }
        }
    }
}
