﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Boter_kaas_eieren_2._0
{
    public partial class frmBKE : Form
    {

        Button[,] btnBKE;
        int Aantal = 30;
		bool turn = true;
		int xTeller = 0;
		int oTeller = 0;

        public frmBKE()
        {
            int Width = ClientSize.Width;
            int Heigth = ClientSize.Height;

            btnBKE = new Button[Aantal,Aantal];

            // Voegt knoppen toe 
            for (int y = 0; y < Aantal; y++)
            {
                for (int x = 0; x < Aantal; x++)
                {
                    btnBKE[x, y] = new Button();
                    btnBKE[x, y].Size = new Size(Width/Aantal, Heigth/Aantal);
                    btnBKE[x, y].Location = new Point(Width*x/Aantal, Heigth*y/Aantal);
                    btnBKE[x, y].Click += new EventHandler(this.btnBKE_Click);
                    this.Controls.Add(btnBKE[x,y]);
                }
            }
        }


        // X is aan de beurt en daarna is O aan de beurt
        private void btnBKE_Click(object sender, EventArgs e)
        {

            Button bTurn = (Button)sender;
            if (turn)
            {
                bTurn.Text = "X";
            }

            else
            {
                bTurn.Text = "O";
            }

            turn = !turn;
            bTurn.Enabled = false;

			checkFullNumber("X", ref xTeller);
			checkFullNumber("O", ref oTeller);
			checkRow("X");
			checkRow("O");
        }

		// kijkt of de aantal X'en en O'en klopt
		private bool checkFullNumber(string letter, ref int teller)
        {
			teller = 0;

			// kijkt naar aantal X'en O'en
            for (int y = 0; y < Aantal; y++)
            {
                for (int x = 0; x < Aantal; x++)
                {
					if (btnBKE[x, y].Text == letter)
                    {
						teller++;
                    }

					if (teller == Aantal)
                    {
						return true;
                    }
                }
            }
			return false;
        }

		// kijkt naar alle win mogelijkheiden
		private bool checkRow(string letter2)
		{
			int tempTell = 0;
			//kijkt horizontaal of de X wint of de O wint
			for (int y = 0; y < Aantal; y++)
			{
				for (int x = 0; x < Aantal; x++)
				{
					if (btnBKE[x, y].Text == letter2)
					{
						tempTell++;
					}
					if (tempTell == Aantal)
					{
						MessageBox.Show(letter2 + " wint");
					}
				}
				tempTell = 0;
			}

			//kijkt vertikaal
			for (int x = 0; x < Aantal; x++)
			{
				for (int y = 0; y < Aantal; y++)
				{
					if (btnBKE[x, y].Text == letter2)
					{
						tempTell++;
					}
					if (tempTell == Aantal)
					{
						MessageBox.Show(letter2 + " wint");
					}
				}
				tempTell = 0;
			}

			//kijkt diagonaal
			for (int x = 0; x < Aantal; x++)
			{
				if (btnBKE[x, x].Text == letter2)
				{
					tempTell++;
				}
				if (tempTell == Aantal)
				{
					MessageBox.Show(letter2 + " wint");
				}

			}
			tempTell = 0;

			//kijkt anti diagonaal
			for (int x = 0; x < Aantal; x++)
			{
				//MessageBox.Show(((Aantal-1) - x).ToString());
			
				if (btnBKE[x, (Aantal-1) - x].Text == letter2)
				{
					tempTell++;
				}
				if (tempTell == Aantal)
				{
					MessageBox.Show(letter2 + " wint");
				}
			}

			return false;
		}
    }
}
